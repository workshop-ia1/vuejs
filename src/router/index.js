import Vue from 'vue'
import VueRouter from 'vue-router'
import Maps from '../views/Maps.vue'
import Graphics from '../views/Graphics.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Maps',
    component: Maps
  },
  {
    path: '/graphics',
    name: 'Graphics',
    component: Graphics
   }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
