import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {	
  	ligne: null,
  	arret: null,
  	heure: null,
  	minutes: null,
  	date: null,
  },
  mutations: {
  	updateDate (state, date) {
    state.date = date
  },
  	updateHeure (state, heure) {
    state.heure = heure
  },
  	updateArret (state, ligne) {
    state.arret = ligne
  },
  	updateMinutes (state, minutes) {
    state.minutes = minutes
  }
  },
  actions: {
  },
  modules: {
  }
})
